import { Children, component, React, Style } from 'local/react/component'

export const defaultSectionStyle: Style = {
  display: 'flex',
  justifyContent: 'center',
  padding: '0.5em 0'
}

interface ISectionProps {
  across?: boolean
  center?: boolean
  children: Children
  className?: string
  indent?: boolean
  style?: Style
}

export const sectionIndentWrapperStyle: Style = {
  display: 'flex',
  flexDirection: 'row'
}

export const sectionIndentGuideStyle: Style = {
  borderRadius: '0.25em',
  width: '1em',
  backgroundColor: 'rgba(255, 255, 255, 0.2)',
  margin: '0.75em 1em 0.75em 0',
  flexShrink: 0
}

export const Section = component.children
  .props<ISectionProps>()
  .render(({ indent, ...props }) =>
    indent ? (
      <div style={sectionIndentWrapperStyle}>
        <div style={sectionIndentGuideStyle} />
        {sectionRenderer(props)}
      </div>
    ) : (
      sectionRenderer(props)
    )
  )

const sectionRenderer = ({
  across,
  center,
  children,
  className,
  style
}: ISectionProps) => (
  <div
    style={
      style || {
        ...defaultSectionStyle,
        flexDirection: across ? 'row' : 'column',
        alignItems: center ? 'center' : 'flex-start'
      }
    }
    className={className}
  >
    {children}
  </div>
)
